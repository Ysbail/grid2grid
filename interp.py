import itertools
import collections
import numpy as np
import matplotlib.pyplot as plt

from tqdm import tqdm
from sklearn.metrics.pairwise import haversine_distances

np.random.seed(0)

EARTH_RADIUS = 6371. # km
MAX_KM_WITH_1_DEGREE = 55.6 # 1 degree distances near lat 60 in km

class InterpolateKernel:
    def __init__(self, region, in_res, out_res, radius, adw_kw={'CDD': 800., 'm': 2}, idw_kw={'p': 2}):
        self.region = region # (lat0, lon0, lat1, lon1)
        assert in_res > out_res, "output resolution needs to be greater and the input's"
        self.in_res = in_res
        self.out_res = out_res
        self.radius = radius
        
        self._adw_kernel_map = MapDict()
        self._idw_kernel_map = MapDict()
        self._max_distance_given_radius = int((radius / MAX_KM_WITH_1_DEGREE) / in_res) + 1
        
        self._gen_lats = lambda lat: np.arange(lat - self._max_distance_given_radius * self.in_res,
                                               lat + self._max_distance_given_radius * self.in_res + self.in_res / 2,
                                               self.in_res)
        self._gen_lons = lambda lon: np.arange(lon - self._max_distance_given_radius * self.in_res,
                                               lon + self._max_distance_given_radius * self.in_res + self.in_res / 2,
                                               self.in_res)
        
        self._in_lats = np.arange(self.region[0], self.region[2] + self.in_res / 2, self.in_res)
        self._in_lons = np.arange(self.region[1], self.region[3] + self.in_res / 2, self.in_res)
        
        self._out_lats = np.arange(self.region[0], self.region[2] + self.out_res / 2, self.out_res)
        self._out_lons = np.arange(self.region[1], self.region[3] + self.out_res / 2, self.out_res)
        
        self._lons_delta = np.arange(-self.in_res / 2, self.in_res / 2 + self.out_res / 2, self.out_res).round(3)
        
        self._generate_adw_kernel(**adw_kw)
        self._generate_idw_kernel(**idw_kw)
        
    def _generate_adw_kernel(self, CDD, m):
        _size = self._max_distance_given_radius * 2 + 1
        _base_kernel = np.full((_size, _size), 0.)
        _valid = np.arange(self._gen_lats(0.).size * self._gen_lons(0.).size)
        
        for _lat in self._out_lats:
            for _lon_delta in self._lons_delta:
                _kernel = _base_kernel.copy()
                _lat_nearest = self._in_lats[np.argmin(abs(self._in_lats - _lat))]
                _station_points = np.array(list(itertools.product(self._gen_lats(_lat_nearest), self._gen_lons(0.))))
                
                _grid_point = [[_lat, _lon_delta]]
                
                dists = haversine_distances(np.radians(_grid_point), np.radians(_station_points)) * EARTH_RADIUS
                w = np.exp(-dists / CDD) ** m
                
                _local_valid = _valid[dists[0] <= self.radius]
                _local_invalid = _valid[dists[0] > self.radius]
                _local_stations = _station_points[tuple(_local_valid), :]
                _n_stations = _local_stations.shape[0]
                
                _station_dists = haversine_distances(np.radians(_local_stations), np.radians(_local_stations)) * EARTH_RADIUS
                dists = dists[:, tuple(_local_valid)]
                
                a, b, c = dists[:, :, None] / EARTH_RADIUS, dists[:, None, :] / EARTH_RADIUS, _station_dists[None] / EARTH_RADIUS
                
                _diag_null = np.ones((_n_stations, _n_stations))
                _diag_null[np.diag_indices_from(_diag_null)] = np.nan
                _w = w[:, tuple(_local_valid)]
                
                _sin_sin = np.sin(a) * np.sin(b)
                _sin_sin[_sin_sin == 0.] = 1.
                
                p = np.nansum(_diag_null[None] * (1. - (np.cos(c) - np.cos(a) * np.cos(b)) / _sin_sin), axis=-1)
                q = np.nansum(_w[:, None].repeat(_n_stations, axis=1) * _diag_null[None], axis=-1)
                
                a = p / q
                a[np.isnan(a)] = 0.

                w[:, tuple(_local_valid)] *= 1 + a
                w[:, tuple(_local_invalid)] = 0.
                
                self._adw_kernel_map[_lat, _lon_delta] = w.reshape((_size, _size))
        
    def _generate_idw_kernel(self, p):
        _size = self._max_distance_given_radius * 2 + 1
        _base_kernel = np.full((_size, _size), 0.)
        
        for _lat in self._out_lats:
            for _lon_delta in self._lons_delta:
                _kernel = _base_kernel.copy()
                _lat_nearest = self._in_lats[np.argmin(abs(self._in_lats - _lat))]
                _station_points = list(itertools.product(self._gen_lats(_lat_nearest), self._gen_lons(0.)))
                
                _grid_point = [[_lat, _lon_delta]]
                
                dists = haversine_distances(np.radians(_grid_point), np.radians(_station_points)) * EARTH_RADIUS
                dists[dists == 0.] = 1.
                
                w = (1 / dists) ** p
                w[dists > self.radius] = 0.
                
                self._idw_kernel_map[_lat, _lon_delta] = w.reshape((_size, _size))
    
    def _apply_regrid(self, grid, _map_attr):
        _out_grid = np.zeros((self._out_lats.size, self._out_lons.size))
        _size = self._max_distance_given_radius * 2 + 1
        _expanded_grid = np.pad(grid, ((self._max_distance_given_radius, self._max_distance_given_radius),
                                       (self._max_distance_given_radius, self._max_distance_given_radius)), constant_values=np.nan)
        
        print('out grid', _out_grid.shape)
        print('expanded grid', _expanded_grid.shape)
        
        for (_lat, _lon), (i, j) in tqdm(zip(itertools.product(self._out_lats, self._out_lons), itertools.product(np.arange(self._out_lats.size), np.arange(self._out_lons.size)))):
            _lon_delta = round(_lon - self._in_lons[np.argmin(abs(_lon - self._in_lons))], 3)
            w = getattr(self, _map_attr)[_lat, _lon_delta]
            
            _i_near, _j_near = np.argmin(abs(_lat - self._in_lats)), np.argmin(abs(_lon - self._in_lons))
            _local_grid = _expanded_grid[_i_near: _i_near + _size, _j_near: _j_near + _size]
            
            v = np.nansum(_local_grid * w) / np.nansum(~np.isnan(_local_grid) * w)
            _out_grid[i, j] = v
            
        return _out_grid
    
    def adw(self, grid):
        return self._apply_regrid(grid, '_adw_kernel_map')
        
    def idw(self, grid):
        return self._apply_regrid(grid, '_idw_kernel_map')
        
        
class MapDict:
    def __init__(self, ):
        self._lat_keys = None
        self._lon_delta_keys = collections.defaultdict(np.ndarray)
        self._internal = collections.defaultdict(dict)
        
    def __setitem__(self, args, value):
        self._internal[args[0]][args[1]] = value
        self._lat_keys = np.array(list(self._internal.keys()))
        self._lon_delta_keys[args[0]] = np.array(list(self._internal[args[0]].keys()))
        
    def __getitem__(self, args):
        _nearest_key_0 = np.argmin(abs(args[0] - self._lat_keys))
        _key_0 = self._lat_keys[_nearest_key_0]
        
        _nearest_key_1 = np.argmin(abs(args[1] - self._lon_delta_keys[_key_0]))
        _key_1 = self._lon_delta_keys[_key_0][_nearest_key_1]
        
        return self._internal[_key_0][_key_1]

'''
region = [-60, -83.5, 13, -33]
in_res = 1. # input resolution
out_res = 0.25 # target resolution
_radius = 400. # radius in km of 'stations' you want to use

interp = InterpolateKernel(region, in_res, out_res, radius=_radius)

lats = np.arange(-60, 13 + in_res / 2, in_res)
lons = np.arange(-83.5, -33 + in_res / 2, in_res)

grid = np.random.rand(lats.size, lons.size) # grid with the size of the given region, including both ends

grid_adw = interp.adw(grid)
grid_idw = interp.idw(grid)

'''




'''
# DEBUG        
# lats = np.arange(-60, 13.1, res)
# lons = np.arange(-83.5, -32.9, res)
# square_size = 3
# _station_res = 1.
# _target_res = _station_res / 10.
# _radius = _station_res * square_size * np.sqrt(2) / 2 * 110. # km

square_size = 3
region = [-60, -83.5, 13, -33]
in_res = 1.
out_res = 0.1
_radius = in_res * square_size * np.sqrt(2) / 2 * 110. # km
_radius = 400.
test = InterpolateKernel(region, in_res, out_res, radius=_radius)

print(test._in_lats.size, test._in_lons.size)


def gen_grid(res=0.1):
    lats = np.arange(-60, 13.1, res)
    lons = np.arange(-83.5, -32.9, res)
    grid_lats, grid_lons = np.meshgrid(lats, lons, indexing='ij')
    # alts = np.linspace(0, 2000, lats.size * lons.size)
    alts = np.random.randint(0, 201, size=lats.size * lons.size)
    
    return np.c_[grid_lats.flatten(), grid_lons.flatten(), alts.flatten()], lats, lons

grid, lats, lons = gen_grid(in_res)

grid = grid[:, 2].reshape(lats.size, lons.size)

print(grid.shape, lats.size, lons.size)

res1 = test.adw(grid)
res2 = test.idw(grid)

plt.subplot(1, 3, 1)
plt.pcolormesh(lons, lats, grid, cmap='cividis', shading='auto', vmin=grid.min(), vmax=grid.max())
plt.title('orig')
plt.colorbar()

plt.subplot(1, 3, 2)
plt.pcolormesh(test._out_lons, test._out_lats, res1, cmap='cividis', shading='auto', vmin=grid.min(), vmax=grid.max())
plt.title('adw')
plt.colorbar()

plt.subplot(1, 3, 3)
plt.pcolormesh(test._out_lons, test._out_lats, res2, cmap='cividis', shading='auto', vmin=grid.min(), vmax=grid.max())
plt.title('idw')
plt.colorbar()

plt.show()
'''